<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title>Pepidas</title>

		<!-- Google font -->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

		<!-- Bootstrap -->
		<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="css/slick.css"/>
		<link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="css/font-awesome.min.css">

		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="css/style.css"/>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript">
			var precio=2800;
			var cantidad=2;
			var precioFinal=cantidas*precio;

		</script>

    </head>
	<body>
		<!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><i class="fa fa-phone"></i> +057 3** *** ****</a></li>
						<li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
						<li><a href="ubicacion.php"><i class="fa fa-map-marker"></i> Pepidas/a></li>
					</ul>
					<ul class="header-links pull-right">
						<li><a href="#"><i class="fa fa-dollar"></i> Pesos</a></li>
						<li><a href="#"><i class="fa fa-user-o"></i> Mi cuenta</a></li>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img src="./img/logo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									<select class="input-select">
										<option value="0">Todos </option>
										<option value="1">nevados</option>
										<option value="1">Churros</option>
										<option value="1">Bebidas Frias</option>
										<option value="1">B. calientes</option>
									</select>
									<input class="input" placeholder="Busca aquí">
									<button class="search-btn">Buscar</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->
<?php include "php/pedido.php"; ?><!--incluye los productos seleccionados-->
						<!-- ACCOUNT -->
						
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		<nav id="navigation">
			<!-- container -->
			<div class="container">
				<!-- responsive-nav -->
				<div id="responsive-nav">
					<!-- NAV -->
					<ul class="main-nav nav navbar-nav">
					<li ><a href="index.php">Inicio</a></li>
						<li class="active"><a href="churros.php">Churros</a></li>
						<li><a href="nevados.php">Nevados</a></li>
						<li><a href="bebidas_frias.php">Bebidas Frias</a></li>
						<li><a href="bebidas_calientes.php">Bebidas Calientes</a></li>
						<li><a href="promociones.php">Promoción del mes</a></li>
						
					</ul>
					<!-- /NAV -->
				</div>
				<!-- /responsive-nav -->
			</div>
			<!-- /container -->
		</nav>
		<!-- /NAVIGATION -->

		<!-- SECTION -->

	 








<?php
$v1 = $_POST['variable1'];
$valor = 0;
?>
<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?>
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Churros tradicionales-($8.200)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/churros.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>churro<br> Tradicionales</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>15 deliciosos churros con un dip elegible entre los cuales se encuentran: <br>Arequipe<BR> Chocolate<br> Caramelo <br> Leche condensada</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>



<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?>
	 <!-- el 2-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Churros mix-(9.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>churros<br> mix</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>5 churros gruesos relleno de los sabores de tu preferencia dentro de los cuales se encuentran: <br> Arequipe <br> Chocolate<br>Leche condensada <br>Caramelo<br>Maracuya <br>Guayaba<br>Fresa<br>Mora</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?>
	 <!-- el 3-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Bacon sticks-($13.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/bacon_sticks.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Bacon<br> Sticks</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>5 churros bañados  en salsa BBQ-Mielmostaza envueltos en tocino ahumado</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?>
	 <!--el 4 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Churros nute-($10.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nute.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Churros<br> Nute</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>5 deliciosos churros gruesos bañados en nutella</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 5 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Dip Adicional-($500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Dip<br> Adicional</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Dip adicional que combinan genial con los churros tradicionales: <br>Arequipe<br>Leche Condensada<br>Chocolate<br>Caramelo</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 6 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Palitos del rancho-($13.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/palitos_rancho.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Palitos del<br> rancho</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>15 churros delgados crujientes acompañados de salchicha ranchera, tocino ahumado y un dip de mielmostaza-BBQ</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?> <!--el 7 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Snow Crunch-($10.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/snow_crunch.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Snow<br> Crunch</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Recipiente comestible con 2 bolas de helado de vainilla y 2 toppings:<br>Hanuta<br>Nutella<br>M&M'S<br>Hershey's cookies n cream<br>Kinder chocolate<br>Fruta(Fresa-Mora-Piña-Cereza-Durazno)</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 8 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">FrutiChurros-($11.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>FrutiChurros<br> </h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>8 churros, 2 bolas de helado, 2 tipos de fruta:<br>Fresa<br>Mora<br>Piña<br>Cereza<br>Durazno</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 9  -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Churros Creamy-($9.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/churro_helado.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Churros<br> Creamy</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>8 deliciosos churros con 2 bolas de helado</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 10 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Brownie-($7.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Brownie</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>1 Brownie</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 11 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Cherry bomb-($6.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/cherry_bomb.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Cherry<br> bomb</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Deliciosa soda de cereza y limon</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 12 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Limonada de coco-($6.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Limonada de <br> Coco</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Deliciosa limonada para calmar la sed.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 13 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Limonada natural-($5.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Limonada <br> Natural</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Deliciosa limonada natural para hidratarte y calmar la sed o acompañar tu comida.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 14 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Jugos naturales-($4.000-5.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Jugos <br> Naturales</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Jugo natural a tu gusto en:<br>Agua:($4.000)<br>En leche($5.000</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 15 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Coca cola-($2.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Coca<br> Cola</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Disfruta de la gaseosa mas vendida en el siglo XX y XXI</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 16 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Botella de agua-(2.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Botella de <br> Agua</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Disfruta de el producto mas natural que te ofrese el planeta al mejor precio.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 17-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Milo frio-($2.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Milo<br> Frio</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Deliciso milo para calmar la sed y pasar el calor.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 18-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Cocoa Sunset-($4.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Cocoa <br> Sunset</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>"por poner"</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 19-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Cappuccino-($3.800)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Cappuccino<br> </h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>"por poner"</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 20-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Mokaccino-($3.800)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Mokaccino<br> </h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>"por poner"</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 21-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Café Americano-($2.600)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/cafe_americano.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Café<br> Americano</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Delicioso café americano para acompañar tu orden.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 22-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Milo caliente-($2.600)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Milo<br> Caliente</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Delicioso milo caliente para calorear una tarde fria.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 23 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Latte-($2.600)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Latte<br></h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>"pendiente"</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 24-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Kumis-($5.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/kumis.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Kumis<br> </h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Delicioso kumis al mejor precio, incluye cuca.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 25 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado Cocuy-($12.400)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/Nevado_cocuy.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado <br> Cocuy</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con helado de vainilla o chocolate<br>Mantequilla de Maní<br>Snickers<br>Durazno<br>Chantillí</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 26-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado Twister-($10.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/Nevado_twister.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado<br> Twister</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br>Nerds<br>Mermelada mora azul</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 27 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado santa isabel-($15.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_santa_isabel.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado<br> Santa Isabel</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br>Helado de vainilla o chocolate<br>Cerezas o Fresas<br>Chantillí<br>Nutella</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 28-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado Rústico-($17.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_rustic.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado <br> Rústico</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br> Mont Blanc<br>Macadamia caramelizada<br>Syrup Hersheys caramelo<br>Almendras</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 29-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado origen-($14.700)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_origen.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado<br> Origen</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br>Crema de whisky<br>Café Molido término medio<br>Granos de café cubiertos de chocolate.</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 30-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado del Huila-($12.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_huila.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado <br> Huila</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br>Helado de vainilla o chocolate<br>Barras kinder<br>M&M'S cubierto en crema chantillí</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!-- el 31-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado del Ruiz-($14.700)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_ruiz.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado <br> Ruiz</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br>Helado de vainillo o chocolate<br>Hersheys cookies and cream<br>Brownie<br>Chantillí<br>Syrup Hersheys chocolate</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 32 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado del Tolima-($12.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_tolima.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado<br> Tolima</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado con:<br>Helado vainilla o chocolate<br>Mora<br>Milkyway<br>Chantillí<br>Syrup de mora</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 33 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado Gold-($21.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_gold.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado<br> Gold</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nuestro nevado mas especial incluye:<br>Helado de vainilla o chocolate<br>Ferrero rocher<br>Polvo de oro comestible de 24k<br>Hanuta<br>Nutella<br>Chantillí</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 34 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Nevado Hungry-($15.500)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/nevado_hungry.png" alt="">
							</div>
							<div class="shop-body">
								<h3>Nevado<br> Hungry</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Nevado que incluye:<br>Teix<br>Nuggets milo<br>Maracuyá</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>


<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 35 OJO-->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">FrutiNevado</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/pendiente.png" alt="">
							</div>
							<div class="shop-body">
								<h3>FrutiNevado</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>"pendiente"</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>

<?php
$valor= $valor+1;
if ($v1==$valor): 
	 ?><!--el 36 -->
<div class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<div class="billing-details">
							<div class="section-title">
								<h3 class="title">Cookie Crunch-($14.000)</h3>
							</div>
							<div class="form-group">
							</div>
						</div>
						<div class="shiping-details">
							<div class="section-title">
								<h3 class="title"></h3>
							</div>
							<div class="input-checkbox">
								<input type="checkbox" id="shiping-address">
										<span></span>
							</div>
						</div>
						<div class="order-notes">
							<div class="section">
			<div class="container">
				<!-- row -->
				<div class="row">
					<!-- shop -->
					<div class="col-md-4 col-xs-6">
						<div class="shop">
							<div class="shop-img">
								<img src="./img/cookie_crunch.jpeg" alt="">
							</div>
							<div class="shop-body">
								<h3>Cookie<br> Crunch</h3>
							
                              </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
					</div>
						<div class="col-md-5 order-details">
						<div class="section-title text-center">
							<h3 class="title">Descripción</h3>
						</div>
							<div class="order-products">
								<div class="order-col">
									<div>Incluye:<br>Kitkat<br>Minichips</div>				
						<a href="#" class="primary-btn order-submit">Agregar a la orden</a>
					</div>
				</div>
			</div>
		</div>
<?php endif ?>













		<!-- NEWSLETTER -->
		<div id="newsletter" class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-12">
						<div class="newsletter">
							<p>Suscribete para ser el primero en  <strong>Conocer nuevas promociones</strong></p>
							
								<form role="form" name="registro" action="php/registrocorreo.php" method="POST">
								<input class="input" id="correo" name="correo"type="correo" placeholder="Ingresa tu correo">
								<button type="submit "class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribirse</button>
								  
							</form>
							<ul class="newsletter-follow">
								<li>
									<a href="https://www.facebook.com//"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="#"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="https://www.instagram.com//?hl=es-la"><i class="fa fa-instagram"></i></a>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		<footer id="footer">
			<!-- top footer -->
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Sobre nosotros</h3>
								<p>insertar descripción breve</p>
								<ul class="footer-links">
									<li><a href="ubicacion.html"><i class="fa fa-map-marker"></i>Pepidas</a></li>
									<li><a href="#"><i class="fa fa-phone"></i>+057 3** *** ****</a></li>
									<li><a href="#"><i class="fa fa-envelope-o"></i>email@email.com</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Categorias</h3>
								<ul class="footer-links">
									<li><a href="nevados.php">Nevados</a></li>
									<li><a href="churros.php">Churros</a></li>
									<li><a href="bebidas_calientes.php">Bebidas calientes</a></li>
									<li><a href="bebidas_frias.php">Bebidas frias</a></li>
									<li><a href="promociones.php">Promociones</a></li>
								</ul>
							</div>
						</div>

						<div class="clearfix visible-xs"></div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Información</h3>
								<ul class="footer-links">
									<li><a href="#">Sobre nosotros</a></li>
									<li><a href="#">Siguenos en redes</a></li>
									<li><a href="#">Politica de la tienda</a></li>
									
									<li><a href="#">Terminos y condiciones</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-xs-6">
							<div class="footer">
								<h3 class="footer-title">Servicios</h3>
								<ul class="footer-links">
									<li><a href="#">Mi cuenta</a></li>
									<li><a href="#">Ver pedido</a></li>
									
									<li><a href="#">Ayuda</a></li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /row -->
				</div>
				<!-- /container -->
			</div>
			<!-- /top footer -->

		
		<!-- jQuery Plugins -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/slick.min.js"></script>
		<script src="js/nouislider.min.js"></script>
		<script src="js/jquery.zoom.min.js"></script>
		<script src="js/main.js"></script>

	</body>

</html>