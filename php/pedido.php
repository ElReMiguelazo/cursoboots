<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
										<i class="fa fa-shopping-cart"></i>
										<span>Pedido</span>
										<div class="qty">3</div>
									</a>
									<div class="cart-dropdown">
										<div class="cart-list">
											<div class="product-widget">
												<div class="product-img">
													<img src="./img/product01.png" alt="">
												</div>
												<div class="product-body">
													<h3 class="product-name"><a href="#">Producto</a></h3>
													<h4 class="product-price"><span class="qty">1x</span>$14.000</h4>
												</div>
												<button class="delete"><i class="fa fa-close"></i></button>
											</div>

											<div class="product-widget">
												<div class="product-img">
													<img src="./img/product02.png" alt="">
												</div>
												<div class="product-body">
													<h3 class="product-name"><a href="#">Producto</a></h3>
													<h4 class="product-price"><span class="qty">2x</span>$10.000</h4>
												</div>
												<button class="delete"><i class="fa fa-close"></i></button>
											</div>
										</div>
										<div class="cart-summary">
											<small>3 Item(s) seleccionados</small>
											<h5>SUBTOTAL: $24.000</h5>
										</div>
										<div class="cart-btns">
											<a href="#">Ver lista</a>
											<a href="ordenar.php">Pedir  <i class="fa fa-arrow-circle-right"></i></a>
										</div>0.
									</div>
								</div>
								<!-- /Cart -->

								<!-- Menu Toogle -->
							
								<!-- /Menu Toogle -->
							</div>
						</div>